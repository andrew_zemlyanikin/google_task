#!/usr/bin/python2.4 -tt
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

# Additional basic string exercises

# D. verbing
# Given a string, if its length is at least 3,
# add 'ing' to its end.
# Unless it already ends in 'ing', in which case
# add 'ly' instead.
# If the string length is less than 3, leave it unchanged.
# Return the resulting string.
def verbing(s):
  if len(s) >= 3:
    if s[:-3] != "ing":
      s = s + "ing"
      print s
    else:
      s = s + "ly"
      print s
  # +++your code here+++
  return s


# E. not_bad
# Given a string, find the first appearance of the
# substring 'not' and 'bad'. If the 'bad' follows
# the 'not', replace the whole 'not'...'bad' substring
# with 'good'.
# Return the resulting string.
# So 'This dinner is not that bad!' yields:
# This dinner is good!
def not_bad(s):
  bad_f = s.find("bad")
  not_f = s.find("not")
  if not_f != -1 and bad_f != -1 and bad_f > not_f:
    s = s[:not_f] + 'good' + s[bad_f+3:]
    print s
  return s
  # +++your code here+++


# F. front_back
# Consider dividing a string into two halves.
# If the length is even, the front and back halves are the same length.
# If the length is odd, we'll say that the extra char goes in the front half.
# e.g. 'abcde', the front half is 'abc', the back half 'de'.
# Given 2 strings, a and b, return a string of the form
#  a-front + b-front + a-back + b-back
def front_back(a, b):
  a_middle = len(a) / 2
  b_middle = len(b) / 2
  if len(a) % 2 == 1:
    a_middle = a_middle + 1
  if len(b) % 2 == 1:
    b_middle = b_middle + 1
  print a[:a_middle] + b[:b_middle] + a[a_middle:] + b[b_middle:]
  return a[:a_middle] + b[:b_middle] + a[a_middle:] + b[b_middle:]
  # +++your code here+++
  return


# Simple provided test() function used in main() to print
# what each function returns vs. what it's supposed to return.
def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


# main() calls the above functions with interesting inputs,
# using the above test() to check if the result is correct or not.
def main():
  print 'verbing'
  verbing('hail')#, 'hailing')
  verbing('swiming')#, 'swimingly')
  verbing('do')#, 'do')

  print
  print 'not_bad'
  not_bad('This movie is not so bad')#, 'This movie is good')
  not_bad('This dinner is not that bad!')#, 'This dinner is good!')
  not_bad('This tea is not hot')#, 'This tea is not hot')
  not_bad("It's bad yet not")#
  # , "It's bad yet not")

  print
  print 'front_back'
  front_back('abcd', 'xy')#, 'abxcdy')
  front_back('abcde', 'xyz')#, 'abcxydez')
  front_back('Kitten', 'Donut')#, 'KitDontenut')

if __name__ == '__main__':
  main()
