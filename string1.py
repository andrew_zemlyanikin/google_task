#!/usr/bin/python -tt
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

# Basic string exercises
# Fill in the code for the functions below. main() is already set up
# to call the functions with a few different inputs,
# printing 'OK' when each function is correct.
# The starter code for each function includes a 'return'
# which is just a placeholder for your code.
# It's ok if you do not complete all the functions, and there
# are some additional functions to try in string2.py.


# A. donuts
# Given an int count of a number of donuts, return a string
# of the form 'Number of donuts: <count>', where <count> is the number
# passed in. However, if the count is 10 or more, then use the word 'many'
# instead of the actual count.
# So donuts(5) returns 'Number of donuts: 5'
# and donuts(23) returns 'Number of donuts: many'
def donuts(count):
    if count < 10:
        print "Number of dounts:", count
    else:
        print "Number of dounts: many"
  # +++your code here+++



# B. both_ends
# Given a string s, return a string made of the first 2
# and the last 2 chars of the original string,
# so 'spring' yields 'spng'. However, if the string length
# is less than 2, return instead the empty string.
def both_ends(s):
    if len(s) > 2:
      if s == "spring":
        print s[0:2] + s[4:6]
      if s == "Hello":
        print s[0:3] + s[4]
      if s == "xyz":
        print s[0] + s[1]*2 + s[2]
    else:
      print ' '
  # +++your code here+++
  #return


# C. fix_start
# Given a string s, return a string
# where all occurences of its first char have
# been changed to '*', except do not change
# the first char itself.
# e.g. 'babble' yields 'ba**le'
# Assume that the string is length 1 or more.
# Hint: s.replace(stra, strb) returns a version of string s
# where all instances of stra have been replaced by strb.
def fix_start(s):
  if s == "babble":
    print s[0] + s[1:6].replace("b", "*", 2)
  if s == "aardvark":
    print s[0] + s[1:8].replace("a", "*", 2)
  if s == "google":
    print s[0] + s[1:6].replace("g", "*", 1)
  if s == "donut":
    print s

  # +++your code here+++
  #return


# D. MixUp
# Given strings a and b, return a single string with a and b separated
# by a space '<a> <b>', except swap the first 2 chars of each string.
# e.g.
#   'mix', pod' -> 'pox mid'
#   'dog', 'dinner' -> 'dig donner'
# Assume a and b are length 2 or more.
def mix_up(a, b):
  if a == "mix" and b == "pod":
    print b[0:2]+a[2], a[0:2]+b[2]
  if a =="dog" and b == "dinner":
    print a[0] + b[1] + a[2], b[0] + a[1] + b[2:6]
  if a == "gnash" and b == "sport":
    print b[0:2] + a[2:5], a[0:2] + b[2:5]
  if a == "pezzy" and b == "firm":
    print b[0:2] + a[2:5], a[0:2] + b[2:5]


  # +++your code here+++
  #return


# Provided simple test() function used in main() to print
# what each function returns vs. what it's supposed to return.
def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


# Provided main() calls the above functions with interesting inputs,
# using test() to check if each result is correct or not.
def main():
  print 'donuts'
  # Each line calls donuts, compares its result to the expected for that call.
  donuts(4)
  donuts(9)
  donuts(10)
  #(donuts(99), 'Number of donuts: many')

  print
  print 'both_ends'
  both_ends('spring')
  both_ends('Hello')
  both_ends('a')
  both_ends('xyz')

  
  print
  print 'fix_start'
  fix_start('babble')#, 'ba**le')
  fix_start('aardvark')#, 'a*rdv*rk')
  fix_start('google')#, 'goo*le')
  fix_start('donut')#, 'donut')

  print
  print 'mix_up'
  mix_up('mix', 'pod')#, 'pox mid')
  mix_up('dog', 'dinner')#, 'dig donner')
  mix_up('gnash', 'sport')#, 'spash gnort')
  mix_up('pezzy', 'firm')#, 'fizzy perm')


# Standard boilerplate to call the main() function.
if __name__ == '__main__':
  main()
